import Chicago from "../../assets/Chicago.jpg"
import Nyc from "../../assets/NYC_blackAndWhite.jpg"
import Tokio from "../../assets/TOKIO_blackAndWhite.jpg"

export default function Carosello(){
    return (
        <>
        <p id="home"></p>
          <div id="carouselExampleCaptions" className="carousel slide" style={{height: "fitContent(60%)"}}>
            <div className="carousel-indicators">
              <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
              <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
              <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>
            <div className="carousel-inner">
              <div className="carousel-item active">
                <img src={Nyc} className="d-block w-100" alt="New York city image"/>
                <div className="carousel-caption d-none d-md-block" >
                  <h5 id="cityName">New York</h5>
                  <p>Atmosphere in New York is beautiful</p>
                </div>
              </div>
              <div className="carousel-item">
                <img src={Chicago} className="d-block w-100" alt="Chicago city image"/>
                <div className="carousel-caption d-none d-md-block">
                  <h5 id="cityName">Chicago</h5>
                  <p>Atmosphere in Chicago is beautiful</p>
                </div>
              </div>
              <div className="carousel-item">
                <img src={Tokio} className="d-block w-100" alt="Tokio city image"/>
                <div className="carousel-caption d-none d-md-block">
                  <h5 id="cityName">Tokio</h5>
                  <p>Atmosphere in Tokio is beautiful</p>
                </div>
              </div>
            </div>
            <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
              <span className="carousel-control-prev-icon" aria-hidden="true"></span>
              <span className="visually-hidden">Previous</span>
            </button>
            <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
              <span className="carousel-control-next-icon" aria-hidden="true"></span>
              <span className="visually-hidden">Next</span>
            </button>
          </div>
        </>
    )
}