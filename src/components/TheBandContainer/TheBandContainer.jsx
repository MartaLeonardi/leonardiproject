import City from "../../assets/city.jpg"
import "../styleCss.css"

export default function TheBandContainer(){
    return (
        <>
    <div id="theBandContainer">
     <h5>THE BAND</h5>
     <small>We love music!</small>
        <div>
            <p> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Id, optio mollitia? Accusantium 
                quae reiciendis perspiciatis quia illo maiores facilis iure? Sequi itaque accusantium, possimus 
                laudantium reprehenderit similique nobis quidem doloribus!Loremconsectetur adipisicing elit. 
                Id, optio mollitia? Accusantium quae reiciendis perspiciatis quia illo maiores facilis iure? 
                Sequi itaque accusantium, possimus 
                laudantium reprehenderit similique nobis quidem doloribus!</p>
        </div>

        <div className="container">
           <div className="row">
                <div className="col-lg-4">
                    <div className="text-center" >
                        <h6>Name</h6>
                        <img src={City} alt="an image of a city" id="cityImageBand"/>
                      </div>
                </div>
                <div className="col-lg-4">
                    <div className="text-center" >
                        <h6>Name</h6>
                        <img src={City} alt="an image of a city" id="cityImageBand"/>
                      </div>
                </div>
                <div className="col-lg-4">
                    <div className="text-center" >
                        <h6>Name</h6>
                        <img src={City} alt="an image of a city" id="cityImageBand"/>
                      </div>
                </div>
           </div>
          </div>

     </div>
        </>
    )
}