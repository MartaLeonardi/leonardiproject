import '../styleCss.css'
import FormContact from './FormContact/FormContact'

export default function Contact(){
    return (
        <>
<div className="container-fluid" id="contact">

<h5>Contact</h5>
<small>We love our fans!</small>
<div className="container text-center">
  <div className="row">
    <div className="col">
      <div id="info">

          <p>Fan?Drop a note</p>
          <p><i className="material-icons">&#xe554;</i>Chicago,US</p>
          <p><i className="material-icons">&#xe554;</i>Phone: +00 1515151515</p>
          <p><i className="material-icons">&#xe554;</i>Email: mail@mail.com</p>

      </div>
    </div>
    <div className="col">
     
        <FormContact></FormContact>

    </div>
  </div>
  
  </div>
</div>
        </>
    )
}