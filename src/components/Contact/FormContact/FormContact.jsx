

export default function FormContact(){
return(
    <>
<div className="container text-center">
          <div className="row">
            <div className="col">
              <input type="text" placeholder="Name" style={{margin: "3px"}}/>
            </div>
            <div className="col">
              <input type="email" placeholder="Email" style={{margin: "3px"}}/>
            </div>
          </div>
          <div className="row">
              <textarea rows="4" cols="40" placeholder="Comment" maxLength="400"></textarea>
          </div>
          <div className="row">
            <div className="col">
              <button type="button" className="btn btn-dark" >Send</button>
            </div>
          </div>
        </div>
    </>
)

}