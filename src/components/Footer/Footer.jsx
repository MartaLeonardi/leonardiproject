import "../styleCss.css"

export default function Footer(){
    return (
        <>
    <div className="container-fluid">
      <footer>
        <nav id="ourContact">
          <small>Copyright &copy; 2024 Test finale HTML-CSS-Bootstrap <br/> @Author Marta Leonardi <br/></small>

          <a href="#home" className="link-opacity-50-hover" style={{float:"right"}}>Torna all'inizio della pagina</a>
          <br/>
          <h6>I nostri social:</h6>
          <a href="https://www.facebook.com/?locale=it_IT">Facebook</a>
          <a href="https://www.instagram.com/accounts/login/">Instagram</a>
          <a href="https://www.youtube.com/">Youtube</a>
        </nav>
        

      </footer>
    </div>
        </>
    )
}