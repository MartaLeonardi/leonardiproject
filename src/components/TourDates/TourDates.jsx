import Card from "./Card/Card"
import "../styleCss.css"

export default function TourDates(){
    return (
        <>
<div id="tourDates">

<h5>TOUR DATES</h5>
<small>Lorem ipsum dolor sit, amet consectetur adipisicing elit<br/> Remember to book your ticket!</small>
<div className="container-fluid">
    <div className="row">
      <div className="col" >
        <div>
            <p>September <small>sold out!</small></p> 
            <hr/>
            <p>October <small>sold out!</small></p> 
            <hr/>
            <p>November<small style={{background: "rgb(61, 152, 226)", borderRadius: "100px", float: "right"}}>3</small></p> 
        </div>
      </div>
    </div>
        
        <Card/>

</div>
</div>
        </>
    )
}