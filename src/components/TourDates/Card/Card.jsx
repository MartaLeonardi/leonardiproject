import Chicago from "../../../assets/Chicago.jpg"
import Nyc from "../../../assets/NYC_blackAndWhite.jpg"
import Tokio from "../../../assets/TOKIO_blackAndWhite.jpg"

export default function Card(){
    return(
<>
<div className="row">
      <div className="col">
        <div className="card" style={{width: "100%",height: "100%"}}>
            <img src={Nyc} className="card-img-top" alt="New york city image"/>
            <div className="card-body">
              <h6 className="card-title">New York</h6>
              <p className="card-text">Friday 27 November 2015</p>
              <button type="button" className="btn btn-dark">Buy ticket</button>
            </div>
          </div>
      </div>
      <div className="col">
        <div className="card" style={{width: "100%",height: "100%"}}>
            <img src={Chicago} className="card-img-top" alt="Chicago image"/>
            <div className="card-body">
              <h6 className="card-title">Chicago</h6>
              <p className="card-text"> Friday 27 November 2015</p>
              <button type="button" className="btn btn-dark">Buy ticket</button>
            </div>
          </div>
      </div>
      <div className="col">
        <div className="card" style={{width: "100%",height: "100%"}}>
            <img src={Tokio} className="card-img-top" alt="Tokio city image"/>
            <div className="card-body">
              <h6 className="card-title">Tokio</h6>
              <p className="card-text">Friday 27 November 2015</p>
                <button type="button" className="btn btn-dark">Buy ticket</button>
            </div>
          </div>
      </div>
    </div>
</>
    )
}