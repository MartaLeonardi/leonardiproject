import './App.css'
import Navbar from './components/Navbar/Navbar'
import Carosello from './components/Carosello/Carosello'
import TheBandContainer from './components/TheBandContainer/TheBandContainer'
import TourDates from './components/TourDates/TourDates'
import Contact from './components/Contact/Contact'
import Footer from './components/Footer/Footer'

function App() {

  return (
    <>
      <Navbar></Navbar>
      <Carosello></Carosello>
      <TheBandContainer></TheBandContainer>
      <TourDates></TourDates>
      <Contact></Contact>
      <Footer></Footer>
    </>
  )
}

export default App
